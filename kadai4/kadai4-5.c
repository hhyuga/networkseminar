#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int i;

	/* ↓この行をコメントアウトしない時とした時とでの動作を比較 */
	setbuf(stdout, NULL);

	for (i = 0; i < 20; i++) {
		printf("*");
		sleep(1);
	}
	printf("\n");

	/* 終了 */
	return 0;
}
