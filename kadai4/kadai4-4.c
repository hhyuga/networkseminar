
#include <stdio.h>

int main(int argc, char *argv[])
{
    unsigned long a = 0x1234;
    unsigned long b;

    printf("a & ff00      = %04x\n", a & 0xff00);
    printf("a & ff00 >> 8 = %04x\n", a & 0xff00 >> 8);

    printf("b = a & ff00, b = %04x\n", b = a & 0xff00);
    printf("b >> 8          = %04x\n", b >> 8);

    return 0;
}
