
#include <stdio.h>

int main(int argc, char *argv[])
{
    int a = 165;

    printf("a = %d\n", a);
    printf("a = %4d\n", a);
    printf("a = %04d\n", a);
    printf("a = %x\n", a);
    printf("a = %4x\n", a);
    printf("a = %04x\n", a);

    return 0;
}
